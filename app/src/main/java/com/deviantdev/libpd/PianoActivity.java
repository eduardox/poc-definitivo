package com.deviantdev.libpd;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import org.puredata.android.io.AudioParameters;
import org.puredata.android.io.PdAudio;
import org.puredata.android.utils.PdUiDispatcher;
import org.puredata.core.PdBase;
import org.puredata.core.utils.IoUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by Eduardo on 23/07/2015.
 */
public class PianoActivity extends ActionBarActivity implements Instrumento {

    private PdUiDispatcher dispatcher;
    Button nota;
    Button nota1;
    Button nota2;
    int opcao;
    Rede rede;

    private void init_pd() throws IOException {
        // Configure the audio glue
        int sampleRate = AudioParameters.suggestSampleRate();
        PdAudio.initAudio(sampleRate, 0, 2, 8, true);
        // Create and install the dispatcher
        dispatcher = new PdUiDispatcher();
        PdBase.setReceiver(dispatcher);
    }

    /**
     * Load the prepared PureData patch from the resources (raw).
     * @throws IOException
     */
    private void load_pd_patch() throws IOException {
        File dir = getFilesDir();
        IoUtils.extractZipResource(getResources().openRawResource(R.raw.pdpatch), dir, true);
        File patchFile = new File( dir, "orquestra.pd" );
        PdBase.openPatch(patchFile.getAbsolutePath());
    }


    @Override
    public void initGui() {

        Intent myIntent = getIntent();
        opcao = myIntent.getIntExtra("opcao",0);

        if (opcao == 2) {
            rede = new Rede(this, 6789);
            rede.receiveMenssage();
        }

        this.nota = (Button) findViewById( R.id.button);
        nota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notaButton(rede);
            }
        });

        this.nota1 = (Button) findViewById( R.id.button2);
        nota1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nota1Button(rede);
            }
        });

        this.nota2 = (Button) findViewById( R.id.button3);
        nota2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nota2Button(rede);
            }
        });
    }

    public  void  notaButton(Rede rede){
        if (opcao == 1){
            tocarSolo("piano",60f);
        }
        else if (opcao == 2){
            try {
                rede.sendMessage("piano/60");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            //implementar o centralizado
        }
    }

    public  void  nota1Button(Rede rede){
        if (opcao == 1){
            tocarSolo("piano",62f);
        }
        else if (opcao == 2){
            try {
                rede.sendMessage("piano/62");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            //implementar o centralizado
        }
    }

    public  void  nota2Button(Rede rede){
        if (opcao == 1){
            tocarSolo("piano",0f);
        }
        else if (opcao == 2){
            try {
                rede.sendMessage("piano/0");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            //implementar o centralizado
        }
    }

    @Override
    public void tocar(String instrumento, Float nota) {
        PdBase.sendFloat(instrumento, nota);
        new rodarBackGround(this).execute();
    }

    public void tocarSolo(String instrumento, Float nota) {
        PdBase.sendFloat(instrumento, nota);
    }

    protected void onCreate( Bundle savedInstanceState ) {

        super.onCreate( savedInstanceState );
        setContentView(R.layout.activity_piano);

        try {
            init_pd();
            load_pd_patch();
        } catch ( IOException e ) {
            finish();
        }

        initGui();
        PdAudio.startAudio(this);
    }


    public boolean onCreateOptionsMenu( Menu menu ) {
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

}
